'''
Created on 09-Jan-2015

@author: Kasim
'''
def isfloat(value):
    try:
        float(value)
        return True
    except ValueError:
        return False

if __name__ == '__main__':
    loopAgain = True
    
    while loopAgain:
        operand1 = ''
        operator = ''
        operand2 = ''
        
        while not isfloat(operand1):
            operand1 = raw_input('Enter First Value: ')
        
        while operator != '+' and operator != '-' and operator != '*' and operator != '/':
            operator = raw_input('Choose one operator among "+, -, *, /": ')
        
        while not isfloat(operand2):
            operand2 = raw_input('Enter Second Value: ')
            
        try:
            print eval(operand1.__str__() + operator + operand2.__str__())
        except ZeroDivisionError, Argument:
            print 'Division by zero occured,', 'Program is exiting\n', Argument
            raise SystemExit
        
        confirm = raw_input('Do you want to Continue? (y/n): ')
        if confirm.lower() == 'n':
            loopAgain = False
