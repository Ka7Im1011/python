'''
Created on 09-Jan-2015

@author: Kasim
'''

def isPrime(num):
    if num < 2:
        return False
    if num == 2:
        return True
    else:
        for div in range(2, num):
            if num % div == 0:
                return False
        return True

if __name__ == '__main__':
    i = int(raw_input('Enter a Number: '))
    if isPrime(i):
        print "you've entered a prime number", i.__str__()
    else:
        print i.__str__(), "isn't a prime number"
