'''
Created on 09-Jan-2015

@author: Kasim
'''

planet = raw_input('Enter Planet name: ').lower()
if planet == 'mercury' or planet == 'venus':
    print 'you\'ve chose inner planet'
elif planet == 'mars' or planet == 'jupiter' or planet == 'saturn' or planet == 'uranus' or planet == 'neptune':
    print 'you\'ve entered outer planet'
else:
    print 'please, enter an acceptable planet name'