'''
Created on 03-Jan-2015

@author: Kasim
'''

if __name__ == '__main__':
    _intVar = int("hello", 36)
    print "_intVar is %d, its type is %s" % (_intVar, type(_intVar))
    _intVar = 1.56
    print "_intVar is %f, its type is %s" % (_intVar, type(_intVar))
    