'''
Created on 03-Jan-2015

@author: Kasim
'''

if __name__ == '__main__':
    intVar = 25
    print intVar
    intVar += 25
    print intVar
    intVar *= 2
    print intVar
    intVar -= 50
    print intVar
    intVar /= 3
    # intVar got trunc
    print intVar