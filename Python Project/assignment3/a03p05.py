'''
Created on 15-Jan-2015

@author: Kasim
'''

d1 = {1 : 'a', 2 : 'b', 3 : 'c'}
d2 = {4 : 'd', 5 : 'f'}

for i in d1.items():
    d2[i[0]] = i[1]

print d2
