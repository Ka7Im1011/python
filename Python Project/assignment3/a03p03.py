'''
Created on 15-Jan-2015

@author: Kasim
'''

list1 = ['a', 1, -5, [2, 3, 5], {'a' : 1, 2 : 'b', 1.0 : 'i'}]

for i in list1:
    if type(i) is dict:
        print i.keys()
        print i.values()
