'''
Created on 15-Jan-2015

@author: Kasim
'''

stdDict = {1 : {101: {'name' : 'kasim', 'dob' : '10 Nov', 'sub' : 'C++/CX', 'per' : ()},
                102: {'name' : 'pratik', 'dob' : '13 May', 'sub' : 'Angular JS', 'per' : ()},
                103: {'name' : 'ankur', 'dob' : '7 Sep', 'sub' : 'Android', 'per' : ()}},
           2 : {201: {'name' : 'kasim', 'dob' : '10 Nov', 'sub' : 'Python', 'per' : (13.97)},
                202: {'name' : 'pratik', 'dob' : '13 May', 'sub' : 'JQuery', 'per' : (5.17)},
                203: {'name' : 'ankur', 'dob' : '7 Sep', 'sub' : 'C#', 'per' : (51.56)}},
           3 : {301: {'name' : 'kasim', 'dob' : '10 Nov', 'sub' : 'Java', 'per' : (56.7)},
                302: {'name' : 'pratik', 'dob' : '13 May', 'sub' : 'Obj - c', 'per' : (56)},
                303: {'name' : 'ankur', 'dob' : '7 Sep', 'sub' : 'Magento', 'per' : (97)}}}

print stdDict